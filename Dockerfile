# Base Image
FROM tomcat:9.0-jdk8

MAINTAINER sachunsky@informatik.uni-leipzig.de

ENV DEBIAN_FRONTEND=noninteractive

ENV LAREX_VERSION="dev"

# Enable Networking on port 8080 (Tomcat)
EXPOSE 8080

RUN apt-get update && apt-get install -y wget

# Copy LAREX app and our config
COPY LAREX/target/Larex.war /usr/local/tomcat/webapps/Larex.war
COPY larex.config /larex.config
ENV LAREX_CONFIG=/larex.config

# Create data directory and grant tomcat permissions
VOLUME /data

RUN sed -i '/<Context>/s,<Context>,<Context><Resources allowLinking="true"/>,' /usr/local/tomcat/conf/context.xml

# can still be mounted in at runtime if needed
#COPY tomcat-users.xml web.xml /usr/local/tomcat/conf/
#RUN chown root.root /usr/local/tomcat/conf/*.xml
