TAGNAME = bertsky/larex

build:
	mvn clean install -f LAREX/pom.xml
	docker build -t $(TAGNAME) .

define HELP
cat <<"EOF"
Targets:
	- build	(re)compile Docker image from sources
	- run	start Docker container

Variables:
	- TAGNAME	name of Docker image to build/run
	currently: "$(TAGNAME)"
	- LOGDIR	host directory to mount into `/var/lib/tomcat8/logs`
	currently: "$(LOGDIR)"
	- DATA		host directory to mount into `/data`
	currently: "$(CURDIR)"
	- ETC		space-separated list of host files
			to bind-mount into `/etc/tomcat8`
			(e.g. for authentication or port setup)
	currently: "$(ETC)"
	- PORT		TCP port for the (host-side) web server
	currently: $(PORT)
EOF
endef
export HELP
help: ; @eval "$$HELP"

DATA ?= $(CURDIR)
ETC ?= server.xml # web.xml tomcat-users.xml
PORT ?= 8081
LOGDIR ?= $(CURDIR)/logs
run:
	mkdir -p $(LOGDIR)
	docker run --rm \
	-p $(PORT):8080 \
	-u `id -u root`:`id -g $$USER` \
	--name larex \
	-v $(DATA):/data \
	$(foreach FILE,$(ETC),--mount type=bind,source=$(CURDIR)/$(FILE),destination=/usr/local/tomcat/conf/$(FILE)) \
	-v $(LOGDIR):/usr/local/tomcat/logs \
	$(TAGNAME)

.PHONY: build run help
