## Ground-Truth Annotation mit Larex

Dieses Repository kapselt [LAREX](https://github.com/bertsky/LAREX) installiert in einem Docker-Container, 
wobei die Anwendung so [konfiguriert](./larex.config) wird, daß:
- `/data` das Basisverzeichnis für die _Library_ (also alle Dokument-Verzeichnisse) ist
- die Segmentierung im `segmentation`-Modus erfolgt (also sowohl existierende PAGE-Dateien geladen und manuell korrigiert werden können, als auch automatische Segmentierung möglich ist)
- das Speichern der Ergebnisse auf dem Webserver erfolgt
- keine OCR4all-Integration oder dergleichen aktiv ist

### Benutzung

Starten auf TCP/8081:

    docker run --rm \
    -p 8081:8080 \
    -u `id -u root`:`id -g $USER` \
    --name larex \
    -v <DATA_DIR>:/data \
    bertsky/larex

Wobei unter `/data` folgende Verzeichnisstruktur gebraucht wird (also hier `DATA_DIR=databaseFolder`):

> * File Database for loading book data from a folder structure starting by a
> * databaseFolder. Every folder contained in the databaseFolder is seen as a
> * single book with images representing its pages and xml files representing a
> * segmentation for that page.
> * 
> * databaseFolder/
> * ├── <book_name>/ 
> * │    ├── <page_name>.png 
> * │    └── <page_name>.xml
> * └── <book2_name>/
> *      └── …

(Aus OCR-D-Workspaces heraus lassen sich solche Verzeichnisse per `ocrd-export-larex` erzeugen.)

### Makefile

Zur Vereinfachung beim Bauen eines Docker-Images aus den Quellen und beim Starten des Servers gibt es folgende `make`-Ziele:

```
Targets:
	- build	(re)compile Docker image from sources
	- run	start Docker container

Variables:
	- TAGNAME	name of Docker image to build/run
	currently: "bertsky/larex"
	- LOGDIR	host directory to mount into `/var/lib/tomcat8/logs`
	currently: "/home/xbert/unsortiert/arbeit/heyer/SmartHEC/annotation/logs"
	- DATA		host directory to mount into `/data`
	currently: "$PWD"
	- ETC		space-separated list of host files
			to bind-mount into `/etc/tomcat8`
			(e.g. for authentication or port setup)
	currently: "server.xml "
	- PORT		TCP port for the (host-side) web server
	currently: 8081
```

Neben `server.xml` können so z.B. eine eigene `web.xml` und `tomcat-users.xml` (etwa mit HTTP-Zugriffsbeschränkung) eingebunden werden.
